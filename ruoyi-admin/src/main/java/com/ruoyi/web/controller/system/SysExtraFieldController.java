package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.annotation.RepeatSubmit;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.SysExtraField;
import com.ruoyi.system.service.ISysExtraFieldService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 额外字段Controller
 *
 * @author ruoyi
 * @date 2021-12-10
 */
@RestController
@RequestMapping("/system/extraField")
@Api(tags = "额外字段")
public class SysExtraFieldController extends BaseController {
    @Autowired
    private ISysExtraFieldService sysExtraFieldService;

    /**
     * 查询额外字段列表
     */
    @PreAuthorize("@ss.hasPermi('system:extraField:query')")
    @GetMapping("/list")
    @ApiOperation(value = "查询额外字段列表")
    public TableDataInfo list(SysExtraField sysExtraField) {
        sysExtraField.setPageSize(String.valueOf(Integer.MAX_VALUE));
        startPage();
        List<SysExtraField> list = sysExtraFieldService.selectSysExtraFieldList(sysExtraField);
        return getDataTable(list);
    }

    /**
     * 导出额外字段列表
     */
    @PreAuthorize("@ss.hasPermi('system:extraField:export')")
    @Log(title = "额外字段", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    @ApiOperation(value = "导出额外字段列表")
    public AjaxResult export(SysExtraField sysExtraField) {
        List<SysExtraField> list = sysExtraFieldService.selectSysExtraFieldList(sysExtraField);
        ExcelUtil<SysExtraField> util = new ExcelUtil<SysExtraField>(SysExtraField.class);
        return util.exportExcel(list, "name");
    }

    /**
     * 获取额外字段详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:extraField:query')")
    @GetMapping(value = "/{extraFieldId}")
    @ApiOperation(value = "获取额外字段详细信息")
    public AjaxResult getInfo(@PathVariable("extraFieldId") Long extraFieldId) {
        return AjaxResult.success(sysExtraFieldService.selectSysExtraFieldById(extraFieldId));
    }

    /**
     * 新增额外字段
     */
    @RepeatSubmit
    @PreAuthorize("@ss.hasPermi('system:extraField:add')")
    @Log(title = "额外字段", businessType = BusinessType.INSERT)
    @PostMapping
    @ApiOperation(value = "新增额外字段")
    public AjaxResult add(@RequestBody SysExtraField sysExtraField) {
        return toAjax(sysExtraFieldService.insertSysExtraField(sysExtraField));
    }

    /**
     * 修改额外字段
     */
    @RepeatSubmit
    @PreAuthorize("@ss.hasPermi('system:extraField:edit')")
    @Log(title = "额外字段", businessType = BusinessType.UPDATE)
    @PutMapping
    @ApiOperation(value = "修改额外字段")
    public AjaxResult edit(@RequestBody SysExtraField sysExtraField) {
        return toAjax(sysExtraFieldService.updateSysExtraField(sysExtraField));
    }

    /**
     * 删除额外字段
     */
    @RepeatSubmit
    @PreAuthorize("@ss.hasPermi('system:extraField:remove')")
    @Log(title = "额外字段", businessType = BusinessType.DELETE)
    @DeleteMapping("/{extraFieldIds}")
    @ApiOperation(value = "删除额外字段")
    public AjaxResult remove(@PathVariable Long[] extraFieldIds) {
        return toAjax(sysExtraFieldService.deleteSysExtraFieldByIds(extraFieldIds));
    }
}
