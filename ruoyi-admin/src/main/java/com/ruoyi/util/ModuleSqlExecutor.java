package com.ruoyi.util;

import java.io.File;
import java.net.URL;

/**
 * 多模块 sql 执行器
 *
 * @author dpf
 */
public class ModuleSqlExecutor {
    public static void main(String[] args) {
        URL sqlUrl = ModuleSqlExecutor.class.getClassLoader().getResource("classpath*:sql/");
        File sqlFile = new File(sqlUrl.getFile());
        System.out.println(sqlFile);
    }
}
