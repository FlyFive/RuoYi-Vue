<template>
    <!-- 添加或修改${functionName}对话框 -->
    <el-dialog :close-on-click-modal="false" :title="title" :visible.sync="open" width="80vw" append-to-body>
        <el-form ref="form" :model="form" :rules="rules" label-width="150px">
            <form-block-title title="基础信息"/>
            #foreach($column in $columns)
                #set($field=$column.javaField)
                #if($column.insert && !$column.pk)
                    #if(($column.usableColumn) || (!$column.superColumn))
                        #set($parentheseIndex=$column.columnComment.indexOf("（"))
                        #if($parentheseIndex != -1)
                            #set($comment=$column.columnComment.substring(0, $parentheseIndex))
                        #else
                            #set($comment=$column.columnComment)
                        #end
                        #set($dictType=$column.dictType)
                        #if($column.htmlType == "input")
                            <el-form-item label="${comment}" prop="${field}">
                                <el-input v-model="form.${field}" placeholder="请输入${comment}"/>
                            </el-form-item>
                        #elseif($column.htmlType == "select" && "" != $dictType)
                            <el-form-item label="${comment}" prop="${field}">
                                <el-select v-model="form.${field}" placeholder="请选择${comment}" style="width:100%;">
                                    <el-option
                                            v-for="dict in ${field}Options"
                                            :key="dict.dictValue"
                                            :label="dict.dictLabel"
                                            #if($column.javaType == "Integer" || $column.javaType == "Long"):value="parseInt(dict.dictValue)"#else:value="dict.dictValue"#end

                                    ></el-option>
                                </el-select>
                            </el-form-item>
                        #elseif($column.htmlType == "select" && $dictType)
                            <el-form-item label="${comment}" prop="${field}">
                                <el-select v-model="form.${field}" placeholder="请选择${comment}" style="width:100%;">
                                    <el-option label="请选择字典生成" value=""/>
                                </el-select>
                            </el-form-item>
                        #elseif($column.htmlType == "checkbox" && "" != $dictType)
                            <el-form-item label="${comment}" prop="${field}">
                                <el-checkbox-group v-model="form.${field}">
                                    <el-checkbox
                                            v-for="dict in ${field}Options"
                                            :key="dict.dictValue"
                                            :label="dict.dictValue">
                                        {{dict.dictLabel}}
                                    </el-checkbox>
                                </el-checkbox-group>
                            </el-form-item>
                        #elseif($column.htmlType == "checkbox" && $dictType)
                            <el-form-item label="${comment}" prop="${field}">
                                <el-checkbox-group v-model="form.${field}">
                                    <el-checkbox>请选择字典生成</el-checkbox>
                                </el-checkbox-group>
                            </el-form-item>
                        #elseif($column.htmlType == "radio" && "" != $dictType)
                            <el-form-item label="${comment}" prop="${field}">
                                <el-radio-group v-model="form.${field}">
                                    <el-radio
                                            v-for="dict in ${field}Options"
                                            :key="dict.dictValue"
                                            #if($column.javaType == "Integer" || $column.javaType == "Long"):label="parseInt(dict.dictValue)"#else:label="dict.dictValue"#end

                                    >{{dict.dictLabel}}
                                    </el-radio>
                                </el-radio-group>
                            </el-form-item>
                        #elseif($column.htmlType == "radio" && $dictType)
                            <el-form-item label="${comment}" prop="${field}">
                                <el-radio-group v-model="form.${field}">
                                    <el-radio label="1">请选择字典生成</el-radio>
                                </el-radio-group>
                            </el-form-item>
                        #elseif($column.htmlType == "datetime")
                            <el-form-item label="${comment}" prop="${field}">
                                <el-date-picker clearable style="width: 100%;"
                                                v-model="form.${field}"
                                                type="date"
                                                value-format="yyyy-MM-dd"
                                                placeholder="选择${comment}">
                                </el-date-picker>
                            </el-form-item>
                        #elseif($column.htmlType == "textarea")
                            <el-form-item label="${comment}" prop="${field}">
                                <el-input v-model="form.${field}" type="textarea" placeholder="请输入内容"/>
                            </el-form-item>
                        #end
                    #end
                #end
            #end
        </el-form>
        <div slot="footer" class="dialog-footer">
            <el-button @click="submitForm" type="primary">保 存</el-button>
            <el-button @click="cancel">关 闭</el-button>
        </div>
    </el-dialog>
</template>

<script>
    import {get${BusinessName}, add${BusinessName}, update${BusinessName}, export${BusinessName} } from "@/api/${moduleName}/${businessName}";
    import FormBlockTitle from '@/components/ScFormBlockTitle/index';

    export default {
        name: "Edit",
        components: {FormBlockTitle},
        data() {
            return {
                id: -1,
                // 弹出层标题
                title: "录入信息",
                // 是否显示弹出层
                open: false,
                #foreach ($column in $columns)
                    #set($parentheseIndex=$column.columnComment.indexOf("（"))
                    #if($parentheseIndex != -1)
                        #set($comment=$column.columnComment.substring(0, $parentheseIndex))
                    #else
                        #set($comment=$column.columnComment)
                    #end
                    #if(${column.dictType} != '')
                        // $comment字典
                            ${column.javaField}Options: [],
                    #end
                #end
                // 表单参数
                form: {}
                ,
                // 表单校验
                rules: {
            #foreach ($column in $columns)
                #if($column.required)
                    #set($parentheseIndex=$column.columnComment.indexOf("（"))
                    #if($parentheseIndex != -1)
                        #set($comment=$column.columnComment.substring(0, $parentheseIndex))
                    #else
                        #set($comment=$column.columnComment)
                    #end
                    #set($comment=$column.columnComment)
                    $column.javaField: [
                    {required: true, message: "$comment不能为空", trigger: "blur"}
                ]
                    #if($velocityCount != $columns.size()),#end

                #end
            #end
        }
        }
            ;
        },
        created() {
            #foreach ($column in $columns)
                #if(${column.dictType} != '')
                    this.getDicts("${column.dictType}").then(response => {
                        this.${column.javaField}Options = response.data;
                    });
                #end
            #end
        },
        methods: {
            init(id) {
                //编辑页面加载数据
                if (id && id !== -1) {
                    this.id = id;
                    get${BusinessName}(${pkColumn.javaField}).then(response => {
                        this.form = response.data;
                        #foreach ($column in $columns)
                            #if($column.htmlType == "checkbox")
                                this.form.$column.javaField = this.form.${column.javaField}.split(",");
                            #end
                        #end
                    });
                }
            },
            // 取消按钮
            cancel() {
                this.open = false;
                this.reset();
            },
            // 表单重置
            reset() {
                this.form = {
                #foreach ($column in $columns)
                    #if($column.htmlType == "radio")
                        $column.javaField: #if($column.javaType == "Integer" || $column.javaType == "Long")0#else"0"#end#if($velocityCount != $columns.size()),#end

                    #elseif($column.htmlType == "checkbox")
                        $column.javaField: []#if($velocityCount != $columns.size()),#end

                    #else
                        $column.javaField: null#if($velocityCount != $columns.size()),#end

                    #end
                #end
            }
                ;
                this.resetForm("form");
            },
            /** 提交按钮 */
            submitForm() {
                this.#[[$]]#refs["form"].validate(valid => {
                    if (valid) {
                        #foreach ($column in $columns)
                            #if($column.htmlType == "checkbox")
                                this.form.$column.javaField = this.form.${column.javaField}.join(",");
                            #end
                        #end
                        if (this.form.${pkColumn.javaField} != null) {
                            update${BusinessName}(this.form).then(response => {
                                if (response.code === 200) {
                                    this.msgSuccess("修改成功");
                                    this.open = false;
                                    this.$emit('refreshDataList');
                                }
                            });
                        } else {
                            add${BusinessName}(this.form).then(response => {
                                if (response.code === 200) {
                                    this.msgSuccess("新增成功");
                                    this.open = false;
                                    this.$emit('refreshDataList');
                                }
                            });
                        }
                    }
                });
            }
        }
    };
</script>