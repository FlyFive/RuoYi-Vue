-- ----------------------------
-- 1、部门表
-- ----------------------------
drop table if exists sys_dept;
create table sys_dept
(
    dept_id     bigint(20) not null auto_increment comment '部门id',
    parent_id   bigint(20)  default 0 comment '父部门id',
    ancestors   varchar(50) default '' comment '祖级列表',
    dept_name   varchar(30) default '' comment '部门名称',
    dept_attr   varchar(50) default '' comment '部门属性',
    order_num   int(4)      default 0 comment '显示顺序',
    leader      varchar(20) default null comment '负责人',
    phone       varchar(11) default null comment '联系电话',
    email       varchar(50) default null comment '邮箱',
    status      char(1)     default '0' comment '部门状态（0正常 1停用）',
    del_flag    char(1)     default '0' comment '删除标志（0代表存在 2代表删除）',
    create_by   varchar(64) default '' comment '创建者',
    create_time datetime comment '创建时间',
    update_by   varchar(64) default '' comment '更新者',
    update_time datetime comment '更新时间',
    primary key (dept_id)
) engine = innodb
  auto_increment = 200 comment = '部门表';

-- ----------------------------
-- 初始化-部门表数据
-- ----------------------------
insert into sys_dept(dept_id, parent_id, ancestors, dept_name, dept_attr, order_num, leader, phone, email, status, del_flag, create_by, create_time, update_by, update_time)
values (100, 0, '0', '根机构', '', 0, '', '', '', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-09-18 09:41:00'),
       (101, 100, '0,100', '测试公司', '', 99, '', '', '', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-09-18 09:40:06'),
       (103, 101, '0,100,101', '办公室', '', 1, '', '', '', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-09-18 09:39:22'),
       (104, 101, '0,100,101', '财务部', '', 2, '', '', '', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-09-18 09:39:36'),
       (105, 101, '0,100,101', '采购部', '', 3, '', '', '', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-09-18 09:39:43'),
       (106, 101, '0,100,101', '客服部', '', 4, '', '', '', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-09-18 09:39:51'),
       (107, 101, '0,100,101', '第一实验室', 'sys_dept_attr_sys', 5, '', '', '', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-09-18 09:40:06'),
       (200, 101, '0,100,101', '第二实验室', 'sys_dept_attr_sys', 6, '', '', '', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-09-18 09:40:06'),
       (102, 100, '0,100', '知因细胞', '', 1, '', '', '', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-09-18 09:41:00'),
       (108, 102, '0,100,102', '财务部', '', 1, '', '', '', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-09-18 09:41:00'),
       (109, 102, '0,100,102', '技术部', 'sys_dept_attr_sys', 2, '', '', '', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-09-18 09:41:00'),
       (110, 102, '0,100,102', '市场部', '', 3, '', '', '', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-09-18 09:41:00'),
       (111, 102, '0,100,102', '人力资源部', '', 4, '', '', '', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-09-18 09:41:00'),
       (112, 102, '0,100,102', '新生儿销售部', '', 5, '', '', '', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-09-18 09:41:00'),
       (113, 102, '0,100,102', '免疫-销售部', '', 6, '', '', '', '0', '0', 'admin', '2018-03-16 11:33:00', 'admin', '2020-09-18 09:41:00');


-- ----------------------------
-- 2、用户信息表
-- ----------------------------
drop table if exists sys_user;
create table sys_user
(
    user_id     bigint(20)  not null auto_increment comment '用户ID',
    dept_id     bigint(20)   default null comment '部门ID',
    user_name   varchar(30) not null comment '用户账号',
    nick_name   varchar(30) not null comment '用户昵称',
    user_type   varchar(2)   default '00' comment '用户类型（00系统用户）',
    email       varchar(50)  default '' comment '用户邮箱',
    phonenumber varchar(11)  default '' comment '手机号码',
    sex         char(1)      default '0' comment '用户性别（0男 1女 2未知）',
    avatar      varchar(100) default '' comment '头像地址',
    password    varchar(100) default '' comment '密码',
    status      char(1)      default '0' comment '帐号状态（0正常 1停用）',
    del_flag    char(1)      default '0' comment '删除标志（0代表存在 2代表删除）',
    login_ip    varchar(50)  default '' comment '最后登陆IP',
    login_date  datetime comment '最后登陆时间',
    create_by   varchar(64)  default '' comment '创建者',
    create_time datetime comment '创建时间',
    update_by   varchar(64)  default '' comment '更新者',
    update_time datetime comment '更新时间',
    remark      varchar(500) default null comment '备注',
    primary key (user_id)
) engine = innodb
  auto_increment = 100 comment = '用户信息表';

-- ----------------------------
-- 初始化-用户信息表数据
-- ----------------------------
insert into sys_user
values (1, 103, 'admin', '管理员', '00', '00@qq.com', '13000000000', '1', '', '$2a$10$L.DCsP0kAxNFb2ledsfPA.Yio7z7bCrLeAvS0B/J5MTK0NTIRKFVW', '0', '0', '127.0.0.1', '2018-03-16 11-33-00', 'admin',
        '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_user
values (2, 103, 'user', '普通员工', '00', '99@qq.com', '13099999999', '1', '', '$2a$10$L.DCsP0kAxNFb2ledsfPA.Yio7z7bCrLeAvS0B/J5MTK0NTIRKFVW', '0', '0', '127.0.0.1', '2018-03-16 11-33-00', 'admin',
        '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_user
values (3, 104, 'cw', '财务人员', '00', '1@qq.com', '13000000001', '1', '', '$2a$10$L.DCsP0kAxNFb2ledsfPA.Yio7z7bCrLeAvS0B/J5MTK0NTIRKFVW', '0', '0', '127.0.0.1', '2018-03-16 11-33-00', 'admin',
        '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_user
values (4, 105, 'cg', '采购员', '00', '2@qq.com', '13000000002', '1', '', '$2a$10$L.DCsP0kAxNFb2ledsfPA.Yio7z7bCrLeAvS0B/J5MTK0NTIRKFVW', '0', '0', '127.0.0.1', '2018-03-16 11-33-00', 'admin',
        '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_user
values (5, 106, 'dd', '订单计划员', '00', '3@qq.com', '13000000003', '1', '', '$2a$10$L.DCsP0kAxNFb2ledsfPA.Yio7z7bCrLeAvS0B/J5MTK0NTIRKFVW', '0', '0', '127.0.0.1', '2018-03-16 11-33-00', 'admin',
        '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_user
values (6, 107, 'y-zg', '实验室主管', '00', '11@qq.com', '13000000011', '1', '', '$2a$10$L.DCsP0kAxNFb2ledsfPA.Yio7z7bCrLeAvS0B/J5MTK0NTIRKFVW', '0', '0', '127.0.0.1', '2018-03-16 11-33-00', 'admin',
        '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_user
values (7, 107, 'y-jsy', '技术员', '00', '12@qq.com', '13000000012', '1', '', '$2a$10$L.DCsP0kAxNFb2ledsfPA.Yio7z7bCrLeAvS0B/J5MTK0NTIRKFVW', '0', '0', '127.0.0.1', '2018-03-16 11-33-00', 'admin',
        '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_user
values (8, 107, 'y-zkzg', '质控主管', '00', '13@qq.com', '13000000013', '1', '', '$2a$10$L.DCsP0kAxNFb2ledsfPA.Yio7z7bCrLeAvS0B/J5MTK0NTIRKFVW', '0', '0', '127.0.0.1', '2018-03-16 11-33-00', 'admin',
        '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_user
values (9, 107, 'y-zk', '质控人员', '00', '14@qq.com', '13000000014', '1', '', '$2a$10$L.DCsP0kAxNFb2ledsfPA.Yio7z7bCrLeAvS0B/J5MTK0NTIRKFVW', '0', '0', '127.0.0.1', '2018-03-16 11-33-00', 'admin',
        '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_user
values (10, 107, 'y-xbk', '细胞库管理员', '00', '15@qq.com', '13000000015', '1', '', '$2a$10$L.DCsP0kAxNFb2ledsfPA.Yio7z7bCrLeAvS0B/J5MTK0NTIRKFVW', '0', '0', '127.0.0.1', '2018-03-16 11-33-00', 'admin',
        '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_user
values (11, 107, 'y-ck', '仓库管理员', '00', '16@qq.com', '13000000016', '1', '', '$2a$10$L.DCsP0kAxNFb2ledsfPA.Yio7z7bCrLeAvS0B/J5MTK0NTIRKFVW', '0', '0', '127.0.0.1', '2018-03-16 11-33-00', 'admin',
        '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_user
values (15, 107, 'y-sb', '设备管理员', '00', '17@qq.com', '13000000017', '1', '', '$2a$10$L.DCsP0kAxNFb2ledsfPA.Yio7z7bCrLeAvS0B/J5MTK0NTIRKFVW', '0', '0', '127.0.0.1', '2018-03-16 11-33-00', 'admin',
        '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_user
values (12, 107, 'y-zjyy', '制剂应用人员', '00', '18@qq.com', '13000000018', '1', '', '$2a$10$L.DCsP0kAxNFb2ledsfPA.Yio7z7bCrLeAvS0B/J5MTK0NTIRKFVW', '0', '0', '127.0.0.1', '2018-03-16 11-33-00', 'admin',
        '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_user
values (13, 107, 'y-yljg', '医疗结构接收人员', '00', '19@qq.com', '13000000019', '1', '', '$2a$10$L.DCsP0kAxNFb2ledsfPA.Yio7z7bCrLeAvS0B/J5MTK0NTIRKFVW', '0', '0', '127.0.0.1', '2018-03-16 11-33-00', 'admin',
        '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_user
values (20, 109, 'zhaojinfeng', '赵金凤', '00', 'zhaojinfeng@mydnabox.com', '18898369201', '1', '', '$2a$10$L.DCsP0kAxNFb2ledsfPA.Yio7z7bCrLeAvS0B/J5MTK0NTIRKFVW', '0', '0', '127.0.0.1',
        '2020-10-18 11-33-00', 'admin',
        '2020-10-18 11-33-00', 'admin', '2020-10-18 11-33-00', '');
insert into sys_user
values (21, 109, 'liuxiaoping', '刘小萍', '00', 'liuxiaoping@mydnabox.com', '18565285876', '1', '', '$2a$10$L.DCsP0kAxNFb2ledsfPA.Yio7z7bCrLeAvS0B/J5MTK0NTIRKFVW', '0', '0', '127.0.0.1',
        '2020-10-18 11-33-00', 'admin',
        '2020-10-18 11-33-00', 'admin', '2020-10-18 11-33-00', '');
insert into sys_user
values (22, 109, 'liwen', '李稳', '00', 'liwen@mydnabox.com', '18902478828', '0', '', '$2a$10$L.DCsP0kAxNFb2ledsfPA.Yio7z7bCrLeAvS0B/J5MTK0NTIRKFVW', '0', '0', '127.0.0.1', '2020-10-18 11-33-00',
        'admin',
        '2020-10-18 11-33-00', 'admin', '2020-10-18 11-33-00', '');


-- ----------------------------
-- 3、岗位信息表
-- ----------------------------
drop table if exists sys_post;
create table sys_post
(
    post_id     bigint(20)  not null auto_increment comment '岗位ID',
    post_code   varchar(64) not null comment '岗位编码',
    post_name   varchar(50) not null comment '岗位名称',
    post_sort   int(4)      not null comment '显示顺序',
    status      char(1)     not null comment '状态（0正常 1停用）',
    create_by   varchar(64)  default '' comment '创建者',
    create_time datetime comment '创建时间',
    update_by   varchar(64)  default '' comment '更新者',
    update_time datetime comment '更新时间',
    remark      varchar(500) default null comment '备注',
    primary key (post_id)
) engine = innodb comment = '岗位信息表';

-- ----------------------------
-- 初始化-岗位信息表数据
-- ----------------------------


-- ----------------------------
-- 4、角色信息表
-- ----------------------------
drop table if exists sys_role;
create table sys_role
(
    role_id     bigint(20)   not null auto_increment comment '角色ID',
    role_name   varchar(30)  not null comment '角色名称',
    role_key    varchar(100) not null comment '角色权限字符串',
    role_sort   int(4)       not null comment '显示顺序',
    data_scope  char(1)      default '1' comment '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
    status      char(1)      not null comment '角色状态（0正常 1停用）',
    del_flag    char(1)      default '0' comment '删除标志（0代表存在 2代表删除）',
    create_by   varchar(64)  default '' comment '创建者',
    create_time datetime comment '创建时间',
    update_by   varchar(64)  default '' comment '更新者',
    update_time datetime comment '更新时间',
    remark      varchar(500) default null comment '备注',
    primary key (role_id)
) engine = innodb
  auto_increment = 100 comment = '角色信息表';

-- ----------------------------
-- 初始化-角色信息表数据
-- ----------------------------
insert into sys_role
values ('1', '超级管理员', 'admin', 99, 1, '0', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_role
values ('2', '普通角色', 'common', 98, 2, '0', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_role
values ('3', '领导', 'lingdao', 1, 1, '0', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_role
values ('4', '财务', 'caiwu', 2, 1, '0', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_role
values ('5', '采购', 'caigou', 3, 1, '0', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_role
values ('6', '订单计划员', 'dingdanjihuayuan', 4, 1, '0', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_role
values ('7', '技术总监', 'shiyanshizhuren', 5, 1, '0', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_role
values ('8', '实验室主管', 'shiyanshizhuguan', 6, 3, '0', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_role
values ('9', '技术员', 'jishuyuan', 7, 3, '0', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_role
values ('10', '质控主管', 'zhikongzhuguan', 8, 1, '0', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_role
values ('11', '质控员', 'zhikongyuan', 9, 3, '0', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_role
values ('12', '细胞库管理员', 'xibaokuguanliyuan', 10, 3, '0', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_role
values ('13', '仓库管理员', 'cangkuguanliyuan', 11, 3, '0', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_role
values ('16', '设备管理员', 'shebeiguanliyuan', 12, 3, '0', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_role
values ('14', '制剂应用人员', 'zhijiyingyong', 13, 3, '0', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_role
values ('15', '医疗机构接收人员', 'yiliaojigou', 14, 3, '0', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');


-- ----------------------------
-- 5、菜单权限表
-- ----------------------------
drop table if exists sys_menu;
create table sys_menu
(
    menu_id     bigint(20)  not null auto_increment comment '菜单ID',
    menu_name   varchar(50) not null comment '菜单名称',
    parent_id   bigint(20)   default 0 comment '父菜单ID',
    order_num   int(4)       default 0 comment '显示顺序',
    path        varchar(200) default '' comment '路由地址',
    component   varchar(255) default null comment '组件路径',
    is_frame    int(1)       default 1 comment '是否为外链（0是 1否）',
    menu_type   char(1)      default '' comment '菜单类型（M目录 C菜单 F按钮）',
    visible     char(1)      default 0 comment '菜单状态（0显示 1隐藏）',
    status      char(1)      default 0 comment '菜单状态（0正常 1停用）',
    perms       varchar(100) default null comment '权限标识',
    icon        varchar(100) default '#' comment '菜单图标',
    create_by   varchar(64)  default '' comment '创建者',
    create_time datetime comment '创建时间',
    update_by   varchar(64)  default '' comment '更新者',
    update_time datetime comment '更新时间',
    remark      varchar(500) default '' comment '备注',
    primary key (menu_id)
) engine = innodb
  auto_increment = 2000 comment = '菜单权限表';

-- ----------------------------
-- 初始化-菜单信息表数据
-- ----------------------------
-- 一级菜单
insert into sys_menu
values ('1', '系统管理', '0', '96', 'system', null, 1, 'M', '0', '0', '', 'system', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '系统管理目录');
insert into sys_menu
values ('2', '系统监控', '0', '97', 'monitor', null, 1, 'M', '0', '0', '', 'monitor', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '系统监控目录');
insert into sys_menu
values ('3', '系统工具', '0', '98', 'tool', null, 1, 'M', '0', '0', '', 'tool', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '系统工具目录');
-- 二级菜单
insert into sys_menu
values ('100', '用户管理', '1', '1', 'user', 'system/user/index', 1, 'C', '0', '0', 'system:user:list', 'user', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '用户管理菜单');
insert into sys_menu
values ('101', '角色管理', '1', '2', 'role', 'system/role/index', 1, 'C', '0', '0', 'system:role:list', 'peoples', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '角色管理菜单');
insert into sys_menu
values ('102', '菜单管理', '1', '3', 'menu', 'system/menu/index', 1, 'C', '0', '0', 'system:menu:list', 'tree-table', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '菜单管理菜单');
insert into sys_menu
values ('103', '部门管理', '1', '4', 'dept', 'system/dept/index', 1, 'C', '0', '0', 'system:dept:list', 'tree', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '部门管理菜单');
insert into sys_menu
values ('104', '岗位管理', '1', '5', 'post', 'system/post/index', 1, 'C', '0', '0', 'system:post:list', 'post', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '岗位管理菜单');
insert into sys_menu
values ('105', '字典管理', '1', '6', 'dict', 'system/dict/index', 1, 'C', '0', '0', 'system:dict:list', 'dict', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '字典管理菜单');
insert into sys_menu
values ('106', '参数设置', '1', '7', 'config', 'system/config/index', 1, 'C', '0', '0', 'system:config:list', 'edit', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '参数设置菜单');
insert into sys_menu
values ('107', '通知公告', '1', '8', 'notice', 'system/notice/index', 1, 'C', '0', '0', 'system:notice:list', 'message', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '通知公告菜单');
insert into sys_menu
values ('108', '日志管理', '1', '9', 'log', 'system/log/index', 1, 'M', '0', '0', '', 'log', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '日志管理菜单');
insert into sys_menu
values ('109', '在线用户', '2', '1', 'online', 'monitor/online/index', 1, 'C', '0', '0', 'monitor:online:list', 'online', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '在线用户菜单');
insert into sys_menu
values ('110', '定时任务', '2', '2', 'job', 'monitor/job/index', 1, 'C', '0', '0', 'monitor:job:list', 'job', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '定时任务菜单');
insert into sys_menu
values ('111', '数据监控', '2', '3', 'druid', 'monitor/druid/index', 1, 'C', '0', '0', 'monitor:druid:list', 'druid', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '数据监控菜单');
insert into sys_menu
values ('112', '服务监控', '2', '4', 'server', 'monitor/server/index', 1, 'C', '0', '0', 'monitor:server:list', 'server', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '服务监控菜单');
insert into sys_menu
values ('113', '表单构建', '3', '1', 'build', 'tool/build/index', 1, 'C', '0', '0', 'tool:build:list', 'build', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '表单构建菜单');
insert into sys_menu
values ('114', '代码生成', '3', '2', 'gen', 'tool/gen/index', 1, 'C', '0', '0', 'tool:gen:list', 'code', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '代码生成菜单');
insert into sys_menu
values ('115', '系统接口', '3', '3', 'swagger', 'tool/swagger/index', 1, 'C', '0', '0', 'tool:swagger:list', 'swagger', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '系统接口菜单');
-- 三级菜单
insert into sys_menu
values ('500', '操作日志', '108', '1', 'operlog', 'monitor/operlog/index', 1, 'C', '0', '0', 'monitor:operlog:list', 'form', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '操作日志菜单');
insert into sys_menu
values ('501', '登录日志', '108', '2', 'logininfor', 'monitor/logininfor/index', 1, 'C', '0', '0', 'monitor:logininfor:list', 'logininfor', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00',
        '登录日志菜单');
-- 用户管理按钮
insert into sys_menu
values ('1001', '用户查询', '100', '1', '', '', 1, 'R', '0', '0', 'system:user:query', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_menu
values ('1002', '用户新增', '100', '2', '', '', 1, 'F', '0', '0', 'system:user:add', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_menu
values ('1003', '用户修改', '100', '3', '', '', 1, 'F', '0', '0', 'system:user:edit', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_menu
values ('1004', '用户删除', '100', '4', '', '', 1, 'F', '0', '0', 'system:user:remove', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_menu
values ('1005', '用户导出', '100', '5', '', '', 1, 'F', '0', '0', 'system:user:export', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_menu
values ('1006', '用户导入', '100', '6', '', '', 1, 'F', '0', '0', 'system:user:import', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_menu
values ('1007', '重置密码', '100', '7', '', '', 1, 'F', '0', '0', 'system:user:resetPwd', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
-- 角色管理按钮
insert into sys_menu
values ('1008', '角色查询', '101', '1', '', '', 1, 'R', '0', '0', 'system:role:query', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_menu
values ('1009', '角色新增', '101', '2', '', '', 1, 'F', '0', '0', 'system:role:add', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_menu
values ('1010', '角色修改', '101', '3', '', '', 1, 'F', '0', '0', 'system:role:edit', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_menu
values ('1011', '角色删除', '101', '4', '', '', 1, 'F', '0', '0', 'system:role:remove', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_menu
values ('1012', '角色导出', '101', '5', '', '', 1, 'F', '0', '0', 'system:role:export', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
-- 菜单管理按钮
insert into sys_menu
values ('1013', '菜单查询', '102', '1', '', '', 1, 'R', '0', '0', 'system:menu:query', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_menu
values ('1014', '菜单新增', '102', '2', '', '', 1, 'F', '0', '0', 'system:menu:add', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_menu
values ('1015', '菜单修改', '102', '3', '', '', 1, 'F', '0', '0', 'system:menu:edit', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_menu
values ('1016', '菜单删除', '102', '4', '', '', 1, 'F', '0', '0', 'system:menu:remove', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
-- 部门管理按钮
insert into sys_menu
values ('1017', '部门查询', '103', '1', '', '', 1, 'R', '0', '0', 'system:dept:query', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_menu
values ('1018', '部门新增', '103', '2', '', '', 1, 'F', '0', '0', 'system:dept:add', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_menu
values ('1019', '部门修改', '103', '3', '', '', 1, 'F', '0', '0', 'system:dept:edit', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_menu
values ('1020', '部门删除', '103', '4', '', '', 1, 'F', '0', '0', 'system:dept:remove', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
-- 岗位管理按钮
insert into sys_menu
values ('1021', '岗位查询', '104', '1', '', '', 1, 'R', '0', '0', 'system:post:query', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_menu
values ('1022', '岗位新增', '104', '2', '', '', 1, 'F', '0', '0', 'system:post:add', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_menu
values ('1023', '岗位修改', '104', '3', '', '', 1, 'F', '0', '0', 'system:post:edit', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_menu
values ('1024', '岗位删除', '104', '4', '', '', 1, 'F', '0', '0', 'system:post:remove', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_menu
values ('1025', '岗位导出', '104', '5', '', '', 1, 'F', '0', '0', 'system:post:export', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
-- 字典管理按钮
insert into sys_menu
values ('1026', '字典查询', '105', '1', '#', '', 1, 'R', '0', '0', 'system:dict:query', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_menu
values ('1027', '字典新增', '105', '2', '#', '', 1, 'F', '0', '0', 'system:dict:add', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_menu
values ('1028', '字典修改', '105', '3', '#', '', 1, 'F', '0', '0', 'system:dict:edit', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_menu
values ('1029', '字典删除', '105', '4', '#', '', 1, 'F', '0', '0', 'system:dict:remove', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_menu
values ('1030', '字典导出', '105', '5', '#', '', 1, 'F', '0', '0', 'system:dict:export', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
-- 参数设置按钮
insert into sys_menu
values ('1031', '参数查询', '106', '1', '#', '', 1, 'R', '0', '0', 'system:config:query', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_menu
values ('1032', '参数新增', '106', '2', '#', '', 1, 'F', '0', '0', 'system:config:add', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_menu
values ('1033', '参数修改', '106', '3', '#', '', 1, 'F', '0', '0', 'system:config:edit', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_menu
values ('1034', '参数删除', '106', '4', '#', '', 1, 'F', '0', '0', 'system:config:remove', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_menu
values ('1035', '参数导出', '106', '5', '#', '', 1, 'F', '0', '0', 'system:config:export', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
-- 通知公告按钮
insert into sys_menu
values ('1036', '公告查询', '107', '1', '#', '', 1, 'R', '0', '0', 'system:notice:query', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_menu
values ('1037', '公告新增', '107', '2', '#', '', 1, 'F', '0', '0', 'system:notice:add', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_menu
values ('1038', '公告修改', '107', '3', '#', '', 1, 'F', '0', '0', 'system:notice:edit', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_menu
values ('1039', '公告删除', '107', '4', '#', '', 1, 'F', '0', '0', 'system:notice:remove', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
-- 操作日志按钮
insert into sys_menu
values ('1040', '操作查询', '500', '1', '#', '', 1, 'R', '0', '0', 'monitor:operlog:query', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_menu
values ('1041', '操作删除', '500', '2', '#', '', 1, 'F', '0', '0', 'monitor:operlog:remove', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_menu
values ('1042', '日志导出', '500', '4', '#', '', 1, 'F', '0', '0', 'monitor:operlog:export', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
-- 登录日志按钮
insert into sys_menu
values ('1043', '登录查询', '501', '1', '#', '', 1, 'R', '0', '0', 'monitor:logininfor:query', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_menu
values ('1044', '登录删除', '501', '2', '#', '', 1, 'F', '0', '0', 'monitor:logininfor:remove', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_menu
values ('1045', '日志导出', '501', '3', '#', '', 1, 'F', '0', '0', 'monitor:logininfor:export', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
-- 在线用户按钮
insert into sys_menu
values ('1046', '在线查询', '109', '1', '#', '', 1, 'R', '0', '0', 'monitor:online:query', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_menu
values ('1047', '批量强退', '109', '2', '#', '', 1, 'F', '0', '0', 'monitor:online:batchLogout', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_menu
values ('1048', '单条强退', '109', '3', '#', '', 1, 'F', '0', '0', 'monitor:online:forceLogout', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
-- 定时任务按钮
insert into sys_menu
values ('1049', '任务查询', '110', '1', '#', '', 1, 'R', '0', '0', 'monitor:job:query', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_menu
values ('1050', '任务新增', '110', '2', '#', '', 1, 'F', '0', '0', 'monitor:job:add', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_menu
values ('1051', '任务修改', '110', '3', '#', '', 1, 'F', '0', '0', 'monitor:job:edit', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_menu
values ('1052', '任务删除', '110', '4', '#', '', 1, 'F', '0', '0', 'monitor:job:remove', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_menu
values ('1053', '状态修改', '110', '5', '#', '', 1, 'F', '0', '0', 'monitor:job:changeStatus', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_menu
values ('1054', '任务导出', '110', '7', '#', '', 1, 'F', '0', '0', 'monitor:job:export', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
-- 代码生成按钮
insert into sys_menu
values ('1055', '生成查询', '114', '1', '#', '', 1, 'R', '0', '0', 'tool:gen:query', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_menu
values ('1056', '生成修改', '114', '2', '#', '', 1, 'F', '0', '0', 'tool:gen:edit', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_menu
values ('1057', '生成删除', '114', '3', '#', '', 1, 'F', '0', '0', 'tool:gen:remove', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_menu
values ('1058', '导入代码', '114', '2', '#', '', 1, 'F', '0', '0', 'tool:gen:import', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_menu
values ('1059', '预览代码', '114', '4', '#', '', 1, 'F', '0', '0', 'tool:gen:preview', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_menu
values ('1060', '生成代码', '114', '5', '#', '', 1, 'F', '0', '0', 'tool:gen:code', '#', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');


-- ----------------------------
-- 6、用户和角色关联表  用户N-1角色
-- ----------------------------
drop table if exists sys_user_role;
create table sys_user_role
(
    user_id bigint(20) not null comment '用户ID',
    role_id bigint(20) not null comment '角色ID',
    primary key (user_id, role_id)
) engine = innodb comment = '用户和角色关联表';

-- ----------------------------
-- 初始化-用户和角色关联表数据
-- ----------------------------
insert into sys_user_role
values ('1', '1');
insert into sys_user_role
values ('2', '2');
insert into sys_user_role
values ('3', '4');
insert into sys_user_role
values ('4', '5');
insert into sys_user_role
values ('5', '6');
insert into sys_user_role
values ('6', '8');
insert into sys_user_role
values ('7', '9');
insert into sys_user_role
values ('8', '10');
insert into sys_user_role
values ('9', '11');
insert into sys_user_role
values ('10', '12');
insert into sys_user_role
values ('11', '13');
insert into sys_user_role
values ('12', '14');
insert into sys_user_role
values ('13', '15');
insert into sys_user_role
values ('14', '8');
insert into sys_user_role
values ('15', '16');
insert into sys_user_role
values ('20', '13');
insert into sys_user_role
values ('21', '13');
insert into sys_user_role
values ('22', '13');


-- ----------------------------
-- 7、角色和菜单关联表  角色1-N菜单
-- ----------------------------
drop table if exists sys_role_menu;
create table sys_role_menu
(
    role_id bigint(20) not null comment '角色ID',
    menu_id bigint(20) not null comment '菜单ID',
    primary key (role_id, menu_id)
) engine = innodb comment = '角色和菜单关联表';

-- ----------------------------
-- 初始化-角色和菜单关联表数据
-- ----------------------------
insert into `sys_role_menu`(`role_id`, `menu_id`)
values (8, 9),
       (8, 92),
       (8, 96),
       (8, 98),
       (8, 100),
       (8, 910),
       (8, 1001),
       (8, 2001),
       (8, 2115),
       (8, 2120),
       (8, 2143),
       (8, 2144),
       (8, 2145),
       (8, 2146),
       (8, 2147),
       (8, 2148),
       (8, 2157),
       (8, 2158),
       (8, 2159),
       (8, 2160),
       (8, 2161),
       (8, 2162),
       (8, 2164),
       (9, 9),
       (9, 96),
       (9, 98),
       (9, 1001),
       (9, 2001),
       (9, 2115),
       (9, 2120),
       (9, 2143),
       (9, 2144),
       (9, 2145),
       (9, 2146),
       (9, 2147),
       (9, 2148),
       (9, 2157),
       (9, 2158),
       (9, 2159),
       (9, 2160),
       (9, 2161),
       (9, 2162),
       (9, 2163),
       (9, 2164),
       (10, 17),
       (10, 100),
       (10, 172),
       (10, 1001),
       (10, 2001),
       (10, 1715),
       (10, 1716),
       (10, 2098),
       (10, 2099),
       (10, 2100),
       (10, 2101),
       (10, 2102),
       (10, 2103),
       (10, 2104),
       (10, 2105),
       (10, 2106),
       (10, 2107),
       (10, 2108),
       (10, 2115),
       (10, 2120),
       (10, 2165),
       (10, 2167),
       (10, 2171),
       (11, 17),
       (11, 171),
       (11, 2001),
       (11, 2099),
       (11, 2100),
       (11, 2101),
       (11, 2108),
       (11, 2115),
       (11, 2120),
       (11, 2165),
       (11, 2166),
       (11, 2167),
       (11, 2168),
       (11, 2169),
       (11, 2170),
       (11, 2171),
       (13, 9),
       (13, 19),
       (13, 91),
       (13, 92),
       (13, 93),
       (13, 94),
       (13, 95),
       (13, 97),
       (13, 99),
       (13, 191),
       (13, 192),
       (13, 193),
       (13, 194),
       (13, 911),
       (13, 1001),
       (13, 105),
       (13, 1026),
       (13, 1027),
       (13, 1028),
       (13, 1029),
       (13, 1030),
       (13, 2001),
       (13, 2109),
       (13, 2110),
       (13, 2111),
       (13, 2112),
       (13, 2113),
       (13, 2114),
       (13, 2115),
       (13, 2116),
       (13, 2117),
       (13, 2118),
       (13, 2119),
       (13, 2120),
       (13, 2121),
       (13, 2122),
       (13, 2123),
       (13, 2124),
       (13, 2125),
       (13, 2126),
       (13, 2127),
       (13, 2128),
       (13, 2129),
       (13, 2130),
       (13, 2131),
       (13, 2132),
       (13, 2133),
       (13, 2134),
       (13, 2135),
       (13, 2136),
       (13, 2137),
       (13, 2138),
       (13, 2139),
       (13, 2140),
       (13, 2141),
       (13, 2142),
       (13, 2149),
       (13, 2150),
       (13, 2151),
       (13, 2152),
       (13, 2153),
       (13, 2154),
       (13, 2155),
       (13, 2156),
       (13, 2163),
       (13, 2164),
       (13, 2196),
       (13, 2197),
       (13, 2198),
       (13, 2199),
       (13, 2200),
       (13, 2201),
       (13, 2202),
       (13, 2203),
       (13, 2204),
       (13, 2205),
       (13, 2206),
       (13, 2207),
       (13, 2208),
       (13, 2209),
       (13, 2210),
       (13, 2211),
       (13, 2212),
       (13, 2213),
       (13, 2214),
       (13, 2215),
       (13, 2216),
       (13, 2218);

-- ----------------------------
-- 8、角色和部门关联表  角色1-N部门
-- ----------------------------
drop table if exists sys_role_dept;
create table sys_role_dept
(
    role_id bigint(20) not null comment '角色ID',
    dept_id bigint(20) not null comment '部门ID',
    primary key (role_id, dept_id)
) engine = innodb comment = '角色和部门关联表';

-- ----------------------------
-- 初始化-角色和部门关联表数据
-- ----------------------------


-- ----------------------------
-- 9、用户与岗位关联表  用户1-N岗位
-- ----------------------------
drop table if exists sys_user_post;
create table sys_user_post
(
    user_id bigint(20) not null comment '用户ID',
    post_id bigint(20) not null comment '岗位ID',
    primary key (user_id, post_id)
) engine = innodb comment = '用户与岗位关联表';

-- ----------------------------
-- 初始化-用户与岗位关联表数据
-- ----------------------------
insert into sys_user_post
values ('1', '1');
insert into sys_user_post
values ('2', '2');


-- ----------------------------
-- 10、操作日志记录
-- ----------------------------
drop table if exists sys_oper_log;
create table sys_oper_log
(
    oper_id        bigint(20) not null auto_increment comment '日志主键',
    title          varchar(50)   default '' comment '模块标题',
    business_type  int(2)        default 0 comment '业务类型（0其它 1新增 2修改 3删除）',
    method         varchar(100)  default '' comment '方法名称',
    request_method varchar(10)   default '' comment '请求方式',
    operator_type  int(1)        default 0 comment '操作类别（0其它 1后台用户 2手机端用户）',
    oper_name      varchar(50)   default '' comment '操作人员',
    dept_name      varchar(50)   default '' comment '部门名称',
    oper_url       varchar(255)  default '' comment '请求URL',
    oper_ip        varchar(50)   default '' comment '主机地址',
    oper_location  varchar(255)  default '' comment '操作地点',
    oper_param     varchar(2000) default '' comment '请求参数',
    json_result    varchar(2000) default '' comment '返回参数',
    status         int(1)        default 0 comment '操作状态（0正常 1异常）',
    error_msg      varchar(2000) default '' comment '错误消息',
    oper_time      datetime comment '操作时间',
    primary key (oper_id)
) engine = innodb
  auto_increment = 100 comment = '操作日志记录';


-- ----------------------------
-- 11、字典类型表
-- ----------------------------
drop table if exists sys_dict_type;
create table sys_dict_type
(
    dict_id     bigint(20) not null auto_increment comment '字典主键',
    dict_name   varchar(100) default '' comment '字典名称',
    dict_type   varchar(100) default '' comment '字典类型',
    status      char(1)      default '0' comment '状态（0正常 1停用）',
    create_by   varchar(64)  default '' comment '创建者',
    create_time datetime comment '创建时间',
    update_by   varchar(64)  default '' comment '更新者',
    update_time datetime comment '更新时间',
    remark      varchar(500) default null comment '备注',
    primary key (dict_id),
    unique (dict_type)
) engine = innodb
  auto_increment = 100 comment = '字典类型表';

insert into sys_dict_type
values (1, '用户性别', 'sys_user_sex', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '用户性别列表');
insert into sys_dict_type
values (2, '菜单状态', 'sys_show_hide', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '菜单状态列表');
insert into sys_dict_type
values (3, '系统开关', 'sys_normal_disable', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '系统开关列表');
insert into sys_dict_type
values (4, '任务状态', 'sys_job_status', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '任务状态列表');
insert into sys_dict_type
values (5, '任务分组', 'sys_job_group', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '任务分组列表');
insert into sys_dict_type
values (6, '系统是否', 'sys_yes_no', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '系统是否列表');
insert into sys_dict_type
values (7, '通知类型', 'sys_notice_type', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '通知类型列表');
insert into sys_dict_type
values (8, '通知状态', 'sys_notice_status', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '通知状态列表');
insert into sys_dict_type
values (9, '操作类型', 'sys_oper_type', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '操作类型列表');
insert into sys_dict_type
values (10, '系统状态', 'sys_common_status', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '登录状态列表');
insert into sys_dict_type
values (11, '机构属性', 'sys_dept_attr', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '机构属性列表');


-- ----------------------------
-- 12、字典数据表
-- ----------------------------
drop table if exists sys_dict_data;
create table sys_dict_data
(
    dict_code   bigint(20) not null auto_increment comment '字典编码',
    dict_sort   int(4)       default 0 comment '字典排序',
    dict_label  varchar(100) default '' comment '字典标签',
    dict_value  varchar(100) default '' comment '字典键值',
    dict_type   varchar(100) default '' comment '字典类型',
    css_class   varchar(100) default null comment '样式属性（其他样式扩展）',
    list_class  varchar(100) default null comment '表格回显样式',
    is_default  char(1)      default 'N' comment '是否默认（Y是 N否）',
    status      char(1)      default '0' comment '状态（0正常 1停用）',
    is_built_in char(1)      default '0' comment '是否内置（0否 1是）',
    create_by   varchar(64)  default '' comment '创建者',
    create_time datetime comment '创建时间',
    update_by   varchar(64)  default '' comment '更新者',
    update_time datetime comment '更新时间',
    remark      varchar(500) default null comment '备注',
    primary key (dict_code)
) engine = innodb
  auto_increment = 100 comment = '字典数据表';

insert into sys_dict_data
values (1, 1, '男', '0', 'sys_user_sex', '', '', 'Y', '0', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '性别男');
insert into sys_dict_data
values (2, 2, '女', '1', 'sys_user_sex', '', '', 'N', '0', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '性别女');
insert into sys_dict_data
values (3, 3, '保密', '2', 'sys_user_sex', '', '', 'N', '0', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '性别未知');
insert into sys_dict_data
values (4, 1, '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '显示菜单');
insert into sys_dict_data
values (5, 2, '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '隐藏菜单');
insert into sys_dict_data
values (6, 1, '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '正常状态');
insert into sys_dict_data
values (7, 2, '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '停用状态');
insert into sys_dict_data
values (8, 1, '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '正常状态');
insert into sys_dict_data
values (9, 2, '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '停用状态');
insert into sys_dict_data
values (10, 1, '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '默认分组');
insert into sys_dict_data
values (11, 2, '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '系统分组');
insert into sys_dict_data
values (12, 1, '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '系统默认是');
insert into sys_dict_data
values (13, 2, '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '系统默认否');
insert into sys_dict_data
values (14, 1, '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '通知');
insert into sys_dict_data
values (15, 2, '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '公告');
insert into sys_dict_data
values (16, 1, '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '正常状态');
insert into sys_dict_data
values (17, 2, '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '关闭状态');
insert into sys_dict_data
values (18, 1, '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '新增操作');
insert into sys_dict_data
values (19, 2, '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '修改操作');
insert into sys_dict_data
values (20, 3, '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '删除操作');
insert into sys_dict_data
values (21, 4, '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '授权操作');
insert into sys_dict_data
values (22, 5, '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '导出操作');
insert into sys_dict_data
values (23, 6, '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '导入操作');
insert into sys_dict_data
values (24, 7, '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '强退操作');
insert into sys_dict_data
values (25, 8, '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '生成操作');
insert into sys_dict_data
values (26, 9, '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '清空操作');
insert into sys_dict_data
values (27, 1, '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '正常状态');
insert into sys_dict_data
values (28, 2, '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '停用状态');
insert into sys_dict_data
values (29, 1, '实验室', 'sys_dept_attr_sys', 'sys_dept_attr', '', '', 'N', '0', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '正常状态');
insert into sys_dict_data
values (30, 2, '合作医疗机构', 'sys_dept_attr_hzyljg', 'sys_dept_attr', '', '', 'N', '0', '0', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '停用状态');


-- ----------------------------
-- 13、参数配置表
-- ----------------------------
drop table if exists sys_config;
create table sys_config
(
    config_id    int(5) not null auto_increment comment '参数主键',
    config_name  varchar(100) default '' comment '参数名称',
    config_key   varchar(100) default '' comment '参数键名',
    config_value varchar(500) default '' comment '参数键值',
    config_type  char(1)      default 'N' comment '系统内置（Y是 N否）',
    create_by    varchar(64)  default '' comment '创建者',
    create_time  datetime comment '创建时间',
    update_by    varchar(64)  default '' comment '更新者',
    update_time  datetime comment '更新时间',
    remark       varchar(500) default null comment '备注',
    primary key (config_id)
) engine = innodb
  auto_increment = 100 comment = '参数配置表';

insert into sys_config
values (1, '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00',
        '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
insert into sys_config
values (2, '用户管理-账号初始密码', 'sys.user.initPassword', '111111', 'Y', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '初始化密码 111111');
insert into sys_config
values (3, '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '深色主题theme-dark，浅色主题theme-light');


-- ----------------------------
-- 14、系统访问记录
-- ----------------------------
drop table if exists sys_logininfor;
create table sys_logininfor
(
    info_id        bigint(20) not null auto_increment comment '访问ID',
    user_name      varchar(50)  default '' comment '用户账号',
    ipaddr         varchar(50)  default '' comment '登录IP地址',
    login_location varchar(255) default '' comment '登录地点',
    browser        varchar(50)  default '' comment '浏览器类型',
    os             varchar(50)  default '' comment '操作系统',
    status         char(1)      default '0' comment '登录状态（0成功 1失败）',
    msg            varchar(255) default '' comment '提示消息',
    login_time     datetime comment '访问时间',
    primary key (info_id)
) engine = innodb
  auto_increment = 100 comment = '系统访问记录';


-- ----------------------------
-- 15、定时任务调度表
-- ----------------------------
drop table if exists sys_job;
create table sys_job
(
    job_id          bigint(20)   not null auto_increment comment '任务ID',
    job_name        varchar(64)  default '' comment '任务名称',
    job_group       varchar(64)  default 'DEFAULT' comment '任务组名',
    invoke_target   varchar(500) not null comment '调用目标字符串',
    cron_expression varchar(255) default '' comment 'cron执行表达式',
    misfire_policy  varchar(20)  default '3' comment '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
    concurrent      char(1)      default '1' comment '是否并发执行（0允许 1禁止）',
    status          char(1)      default '0' comment '状态（0正常 1暂停）',
    create_by       varchar(64)  default '' comment '创建者',
    create_time     datetime comment '创建时间',
    update_by       varchar(64)  default '' comment '更新者',
    update_time     datetime comment '更新时间',
    remark          varchar(500) default '' comment '备注信息',
    primary key (job_id, job_name, job_group)
) engine = innodb
  auto_increment = 100 comment = '定时任务调度表';

insert into sys_job
values (1, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '0/10 * * * * ?', '3', '1', '1', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_job
values (2, '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(\'ry\')', '0/15 * * * * ?', '3', '1', '1', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');
insert into sys_job
values (3, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', '2018-03-16 11-33-00', 'admin', '2018-03-16 11-33-00', '');


-- ----------------------------
-- 16、定时任务调度日志表
-- ----------------------------
drop table if exists sys_job_log;
create table sys_job_log
(
    job_log_id     bigint(20)   not null auto_increment comment '任务日志ID',
    job_name       varchar(64)  not null comment '任务名称',
    job_group      varchar(64)  not null comment '任务组名',
    invoke_target  varchar(500) not null comment '调用目标字符串',
    job_message    varchar(500) comment '日志信息',
    status         char(1)       default '0' comment '执行状态（0正常 1失败）',
    exception_info varchar(2000) default '' comment '异常信息',
    create_time    datetime comment '创建时间',
    primary key (job_log_id)
) engine = innodb comment = '定时任务调度日志表';


-- ----------------------------
-- 17、通知公告表
-- ----------------------------
drop table if exists sys_notice;
create table sys_notice
(
    notice_id      int(4)      not null auto_increment comment '公告ID',
    notice_title   varchar(50) not null comment '公告标题',
    notice_type    char(1)     not null comment '公告类型（1通知 2公告）',
    notice_content longblob     default null comment '公告内容',
    status         char(1)      default '0' comment '公告状态（0正常 1关闭）',
    create_by      varchar(64)  default '' comment '创建者',
    create_time    datetime comment '创建时间',
    update_by      varchar(64)  default '' comment '更新者',
    update_time    datetime comment '更新时间',
    remark         varchar(255) default null comment '备注',
    primary key (notice_id)
) engine = innodb
  auto_increment = 10 comment = '通知公告表';

-- ----------------------------
-- 初始化-公告信息表数据
-- ----------------------------

-- ----------------------------
-- 18、代码生成业务表
-- ----------------------------
drop table if exists gen_table;
create table gen_table
(
    table_id        bigint(20) not null auto_increment comment '编号',
    table_name      varchar(200) default '' comment '表名称',
    table_comment   varchar(500) default '' comment '表描述',
    class_name      varchar(100) default '' comment '实体类名称',
    tpl_category    varchar(200) default 'crud' comment '使用的模板（crud单表操作 tree树表操作）',
    package_name    varchar(100) comment '生成包路径',
    module_name     varchar(30) comment '生成模块名',
    business_name   varchar(30) comment '生成业务名',
    function_name   varchar(50) comment '生成功能名',
    function_author varchar(50) comment '生成功能作者',
    gen_type        char(1)      default '0' comment '生成代码方式（0zip压缩包 1自定义路径）',
    gen_path        varchar(200) default '/' comment '生成路径（不填默认项目路径）',
    options         varchar(1000) comment '其它生成选项',
    create_by       varchar(64)  default '' comment '创建者',
    create_time     datetime comment '创建时间',
    update_by       varchar(64)  default '' comment '更新者',
    update_time     datetime comment '更新时间',
    remark          varchar(500) default null comment '备注',
    primary key (table_id)
) engine = innodb
  auto_increment = 1 comment = '代码生成业务表';


-- ----------------------------
-- 19、代码生成业务表字段
-- ----------------------------
drop table if exists gen_table_column;
create table gen_table_column
(
    column_id      bigint(20) not null auto_increment comment '编号',
    table_id       varchar(64) comment '归属表编号',
    column_name    varchar(200) comment '列名称',
    column_comment varchar(500) comment '列描述',
    column_type    varchar(100) comment '列类型',
    java_type      varchar(500) comment 'JAVA类型',
    java_field     varchar(200) comment 'JAVA字段名',
    is_pk          char(1) comment '是否主键（1是）',
    is_increment   char(1) comment '是否自增（1是）',
    is_required    char(1) comment '是否必填（1是）',
    is_insert      char(1) comment '是否为插入字段（1是）',
    is_edit        char(1) comment '是否编辑字段（1是）',
    is_list        char(1) comment '是否列表字段（1是）',
    is_query       char(1) comment '是否查询字段（1是）',
    query_type     varchar(200) default 'EQ' comment '查询方式（等于、不等于、大于、小于、范围）',
    html_type      varchar(200) comment '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
    dict_type      varchar(200) default '' comment '字典类型',
    sort           int comment '排序',
    create_by      varchar(64)  default '' comment '创建者',
    create_time    datetime comment '创建时间',
    update_by      varchar(64)  default '' comment '更新者',
    update_time    datetime comment '更新时间',
    primary key (column_id)
) engine = innodb
  auto_increment = 1 comment = '代码生成业务表字段';