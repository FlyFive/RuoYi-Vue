package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.SysExtraField;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 系统额外字段Mapper接口
 *
 * @author ruoyi
 * @date 2021-12-10
 */
public interface SysExtraFieldMapper {
    /**
     * 查询系统额外字段
     *
     * @param extraFieldId 系统额外字段ID
     * @return 系统额外字段
     */
    public SysExtraField selectSysExtraFieldById(Long extraFieldId);

    /**
     * 查询系统额外字段列表
     *
     * @param sysExtraField 系统额外字段
     * @return 系统额外字段集合
     */
    public List<SysExtraField> selectSysExtraFieldList(SysExtraField sysExtraField);

    /**
     * 统计列表数量
     * @param sysExtraField 系统额外字段
     * @return 技术
     */
    public Integer countSysExtraFieldList(SysExtraField sysExtraField);

    /**
     * 新增系统额外字段
     *
     * @param sysExtraField 系统额外字段
     * @return 结果
     */
    public int insertSysExtraField(SysExtraField sysExtraField);

    /**
     * 修改系统额外字段
     *
     * @param sysExtraField 系统额外字段
     * @return 结果
     */
    public int updateSysExtraField(SysExtraField sysExtraField);

    /**
     * 删除系统额外字段
     *
     * @param extraFieldId 系统额外字段ID
     * @return 结果
     */
    public int deleteSysExtraFieldById(Long extraFieldId);

    /**
     * 批量删除系统额外字段
     *
     * @param extraFieldIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSysExtraFieldByIds(Long[] extraFieldIds);

    /**
     * 获取额外字段值
     *
     * @return 结果
     */
    public List<Map<String, Object>> getExtraValues(Long userId, String extraTable);

    /**
     * 新增额外字段值
     *
     * @return 结果
     */
    public int insertExtraValues(@Param("paramsMap") Map<String, Object> params);

    /**
     * 修改额外字段值
     *
     * @return 结果
     */
    public int updateExtraValues(@Param("paramsMap") Map<String, Object> params);

    /**
     * 删除额外字段值
     *
     * @return 结果
     */
    public int deleteExtraValues(@Param("relatedId") Long relatedId);
}
