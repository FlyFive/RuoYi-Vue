package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.domain.SysExtraField;
import com.ruoyi.system.mapper.SysExtraFieldMapper;
import com.ruoyi.system.service.ISysExtraFieldService;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 系统额外字段Service业务层处理
 *
 * @author ruoyi
 * @date 2021-12-10
 */
@Service
public class SysExtraFieldServiceImpl implements ISysExtraFieldService {
    @Autowired
    private SysExtraFieldMapper sysExtraFieldMapper;

    /**
     * 查询系统额外字段
     *
     * @param extraFieldId 系统额外字段ID
     * @return 系统额外字段
     */
    @Override
    public SysExtraField selectSysExtraFieldById(Long extraFieldId) {
        return sysExtraFieldMapper.selectSysExtraFieldById(extraFieldId);
    }

    /**
     * 查询系统额外字段列表
     *
     * @param sysExtraField 系统额外字段
     * @return 系统额外字段
     */
    @Override
    public List<SysExtraField> selectSysExtraFieldList(SysExtraField sysExtraField) {
        return sysExtraFieldMapper.selectSysExtraFieldList(sysExtraField);
    }

    /**
     * 新增系统额外字段
     *
     * @param sysExtraField 系统额外字段
     * @return 结果
     */
    @Override
    public int insertSysExtraField(SysExtraField sysExtraField) {

        //权限相关字段赋值
        SysUser loginUser = SecurityUtils.getLoginUser().getUser();
        sysExtraField.setCreateBy(loginUser.getUserName());
        sysExtraField.setAclCreateById(loginUser.getUserId());
        sysExtraField.setAclDeptId(loginUser.getDeptId());

        return sysExtraFieldMapper.insertSysExtraField(sysExtraField);
    }

    /**
     * 修改系统额外字段
     *
     * @param sysExtraField 系统额外字段
     * @return 结果
     */
    @Override
    public int updateSysExtraField(SysExtraField sysExtraField) {

        //权限相关字段赋值
        SysUser loginUser = SecurityUtils.getLoginUser().getUser();
        sysExtraField.setUpdateBy(loginUser.getUserName());

        return sysExtraFieldMapper.updateSysExtraField(sysExtraField);
    }

    /**
     * 批量删除系统额外字段
     *
     * @param extraFieldIds 需要删除的系统额外字段ID
     * @return 结果
     */
    @Override
    public int deleteSysExtraFieldByIds(Long[] extraFieldIds) {
        return sysExtraFieldMapper.deleteSysExtraFieldByIds(extraFieldIds);
    }

    /**
     * 删除系统额外字段信息
     *
     * @param extraFieldId 系统额外字段ID
     * @return 结果
     */
    @Override
    public int deleteSysExtraFieldById(Long extraFieldId) {
        return sysExtraFieldMapper.deleteSysExtraFieldById(extraFieldId);
    }

    @Override
    public int generateExtraFieldValue(String moduleCode, Long extraRelateId, Map<String, Object> paramMap) {
        SysExtraField searchParam = new SysExtraField();
        searchParam.setExtraTable(moduleCode);
        List<SysExtraField> sysExtraFieldList = this.sysExtraFieldMapper.selectSysExtraFieldList(searchParam);

        if (CollectionUtils.isNotEmpty(sysExtraFieldList)) {
            if (paramMap != null) {
                paramMap.put("extraRelatedId", extraRelateId);
                paramMap.put("extraTable", moduleCode);
                if (paramMap.size() > 0) {
                    return this.sysExtraFieldMapper.insertExtraValues(paramMap);
                } else {
                    return 0;
                }
            } else {
                return 1;
            }
        } else {
            return 1;
        }
    }
}
