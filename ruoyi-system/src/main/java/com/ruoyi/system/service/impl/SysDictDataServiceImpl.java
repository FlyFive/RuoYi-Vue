package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.domain.entity.SysDictData;
import com.ruoyi.common.utils.DictUtils;
import com.ruoyi.system.mapper.SysDictDataMapper;
import com.ruoyi.system.service.ISysDictDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 字典 业务层处理
 * 
 * @author ruoyi
 */
@Service
public class SysDictDataServiceImpl implements ISysDictDataService
{
    @Autowired
    private SysDictDataMapper dictDataMapper;

    /**
     * 根据条件分页查询字典数据
     * 
     * @param dictData 字典数据信息
     * @return 字典数据集合信息
     */
    @Override
    public List<SysDictData> selectDictDataList(SysDictData dictData) {
        //todo 目前通过java实现树状结构，应该通过sql，但是mapper中自关联直接错误 Operation not allowed after ResultSet closed
        List<SysDictData> dictDataList = dictDataMapper.selectDictDataList(dictData);
        for (SysDictData sysDictData : dictDataList) {
            SysDictData parent = this.dictDataMapper.selectDictDataById(sysDictData.getDictCode());
            sysDictData.setParent(parent);

            List<SysDictData> children = this.dictDataMapper.selectDictDataByParentId(sysDictData.getDictCode());
            sysDictData.setChildren(children);
        }
        return dictDataList;
    }

    /**
     * 根据字典类型和字典键值查询字典数据信息
     * 
     * @param dictType 字典类型
     * @param dictValue 字典键值
     * @return 字典标签
     */
    @Override
    public String selectDictLabel(String dictType, String dictValue)
    {
        return dictDataMapper.selectDictLabel(dictType, dictValue);
    }

    /**
     * 根据字典数据ID查询信息
     *
     * @param dictCode 字典数据ID
     * @return 字典数据
     */
    @Override
    public SysDictData selectDictDataById(Long dictCode) {
        return dictDataMapper.selectDictDataById(dictCode);
    }

    @Override
    public List<SysDictData> selectDictDataByDictValue(String dictValue) {
        return this.dictDataMapper.selectDictDataByDictValue(dictValue);
    }

    /**
     * 批量删除字典数据信息
     *
     * @param dictCodes 需要删除的字典数据ID
     * @return 结果
     */
    @Override
    public int deleteDictDataByIds(Long[] dictCodes) {
        int row = dictDataMapper.deleteDictDataByIds(dictCodes);
        if (row > 0)
        {
            DictUtils.clearDictCache();
        }
        return row;
    }

    /**
     * 新增保存字典数据信息
     * 
     * @param dictData 字典数据信息
     * @return 结果
     */
    @Override
    public int insertDictData(SysDictData dictData) {
        if (null != dictData.getParentId()) {
            SysDictData parent = this.selectDictDataById(dictData.getParentId());
            dictData.setDepth(parent.getDepth() + 1);
        } else {
            dictData.setDepth(1);
        }
        int row = dictDataMapper.insertDictData(dictData);
        if (row > 0) {
            DictUtils.clearDictCache();
        }
        return row;
    }

    /**
     * 修改保存字典数据信息
     * 
     * @param dictData 字典数据信息
     * @return 结果
     */
    @Override
    public int updateDictData(SysDictData dictData) {
        if (null != dictData.getParentId()) {
            SysDictData parent = this.selectDictDataById(dictData.getDictCode());
            dictData.setDepth(parent.getDepth() + 1);
        } else {
            dictData.setDepth(1);
        }
        int row = dictDataMapper.updateDictData(dictData);
        if (row > 0) {
            DictUtils.clearDictCache();
        }
        return row;
    }
}
