package com.ruoyi.system.service;

import com.ruoyi.system.domain.SysExtraField;

import java.util.List;
import java.util.Map;

/**
 * 系统额外字段Service接口
 *
 * @author ruoyi
 * @date 2021-12-10
 */
public interface ISysExtraFieldService {
    /**
     * 查询系统额外字段
     *
     * @param extraFieldId 系统额外字段ID
     * @return 系统额外字段
     */
    public SysExtraField selectSysExtraFieldById(Long extraFieldId);

    /**
     * 查询系统额外字段列表
     *
     * @param sysExtraField 系统额外字段
     * @return 系统额外字段集合
     */
    public List<SysExtraField> selectSysExtraFieldList(SysExtraField sysExtraField);

    /**
     * 新增系统额外字段
     *
     * @param sysExtraField 系统额外字段
     * @return 结果
     */
    public int insertSysExtraField(SysExtraField sysExtraField);

    /**
     * 修改系统额外字段
     *
     * @param sysExtraField 系统额外字段
     * @return 结果
     */
    public int updateSysExtraField(SysExtraField sysExtraField);

    /**
     * 批量删除系统额外字段
     *
     * @param extraFieldIds 需要删除的系统额外字段ID
     * @return 结果
     */
    public int deleteSysExtraFieldByIds(Long[] extraFieldIds);

    /**
     * 删除系统额外字段信息
     *
     * @param extraFieldId 系统额外字段ID
     * @return 结果
     */
    public int deleteSysExtraFieldById(Long extraFieldId);

    /**
     * 生成扩展字段值
     *
     * @param moduleCode    模块code（和表名保持一致）
     * @param extraRelateId 业务ID
     * @param paramMap      扩展字段实际值
     * @return 结果
     */
    public int generateExtraFieldValue(String moduleCode, Long extraRelateId, Map<String, Object> paramMap);
}
