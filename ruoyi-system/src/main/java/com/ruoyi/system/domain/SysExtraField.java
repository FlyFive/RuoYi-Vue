package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;


/**
 * 系统额外字段表 sys_extra_field_name
 *
 * @author ruoyi
 */
public class SysExtraField extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @Excel(name = "额外字段名id", cellType = Excel.ColumnType.NUMERIC)
    private Long id;

    /**
     * 额外字段对应表名
     */
    @Excel(name = "额外字段对应表名")
    private String extraTable;

    /**
     * 额外字段类型
     */
    @Excel(name = "额外字段类型")
    private String extraType;

    /**
     * 额外字段名
     */
    @Excel(name = "额外字段类型")
    private String extraName;

    /**
     * 额外字段中文名
     */
    @Excel(name = "额外字段中文名")
    private String extraChinese;

    /**
     * 额外字段字典
     */
    @Excel(name = "额外字段字典")
    private String extraDict;

    /**
     * 是否必填
     */
    @Excel(name = "是否必填")
    private String required;

    /**
     * 前缀
     */
    @Excel(name = "前缀")
    private String prefixText;

    /**
     * 后缀
     */
    @Excel(name = "后缀")
    private String suffixText;

    /**
     * 备选项
     */
    @Excel(name = "备选项")
    private String optionList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getExtraTable() {
        return extraTable;
    }

    public void setExtraTable(String extraTable) {
        this.extraTable = extraTable;
    }

    public String getExtraType() {
        return extraType;
    }

    public void setExtraType(String extraType) {
        this.extraType = extraType;
    }

    public String getExtraName() {
        return extraName;
    }

    public void setExtraName(String extraName) {
        this.extraName = extraName;
    }

    public String getExtraChinese() {
        return extraChinese;
    }

    public void setExtraChinese(String extraChinese) {
        this.extraChinese = extraChinese;
    }

    public String getExtraDict() {
        return extraDict;
    }

    public void setExtraDict(String extraDict) {
        this.extraDict = extraDict;
    }

    public String getRequired() {
        return required;
    }

    public void setRequired(String required) {
        this.required = required;
    }

    public String getPrefixText() {
        return prefixText;
    }

    public void setPrefixText(String prefixText) {
        this.prefixText = prefixText;
    }

    public String getSuffixText() {
        return suffixText;
    }

    public void setSuffixText(String suffixText) {
        this.suffixText = suffixText;
    }

    public String getOptionList() {
        return optionList;
    }

    public void setOptionList(String optionList) {
        this.optionList = optionList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("extraTable", extraTable)
                .append("extraType", extraType)
                .append("extraName", extraName)
                .append("extraChinese", extraChinese)
                .append("extraDict", extraDict)
                .append("required", required)
                .append("prefixText", prefixText)
                .append("suffixText", suffixText)
                .append("optionList", optionList)
                .toString();
    }
}
