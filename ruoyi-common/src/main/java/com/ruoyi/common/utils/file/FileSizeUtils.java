package com.ruoyi.common.utils.file;

import java.text.DecimalFormat;

/**
 * 文件大小转换 工具类
 *
 * @author dpf
 */
public class FileSizeUtils {
    /**
     * 字节转kb/mb/gb
     *
     * @param size 文件大小（单位：比特）
     * @return
     */
    public static String getPrintSize(long size) {
        DecimalFormat df = new DecimalFormat("#.00");
        String fileSizeString = "";
        if (size < 1024) {
            fileSizeString = df.format((double) size) + "B";
        } else if (size < 1048576) {
            fileSizeString = df.format((double) size / 1024) + "K";
        } else if (size < 1073741824) {
            fileSizeString = df.format((double) size / 1048576) + "M";
        } else {
            fileSizeString = df.format((double) size / 1073741824) + "G";
        }
        return fileSizeString;
    }

    public static void main(String[] args) {
        String printSize = FileSizeUtils.getPrintSize(1768);
        System.out.println(printSize);
    }
}
