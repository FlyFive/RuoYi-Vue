package com.ruoyi.common.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 读取项目相关配置
 * 
 * @author ruoyi
 */
@Component
@ConfigurationProperties(prefix = "ruoyi")
public class RuoYiConfig
{
    /** 项目名称 */
    private String name;

    /** 版本 */
    private String version;

    /** 版权年份 */
    private String copyrightYear;

    /**
     * 实例演示开关
     */
    private boolean demoEnabled;

    /**
     * 上传路径
     */
    private static String profile;

    /**
     * 获取地址开关
     */
    private static boolean addressEnabled;

    /**
     * 上传图片压缩 开关
     */
    private static boolean uploadImageCompressEnabled = false;
    /**
     * 上传图片压缩 限制
     * 默认的压缩图片字节限制 100K
     */
    private static double uploadImageCompressByteLimit = 100000;

    /**
     * 上传pdf压缩 开关
     */
    private static boolean uploadPdfCompressEnabled = false;
    /**
     * 上传pdf压缩 限制
     * 默认的压缩pdf字节限制 1M
     */
    private static double uploadPdfCompressLimit = 1000000;
    /**
     * 上传pdf压缩 比例
     */
    private static double uploadPdfCompressFactor = 1;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion()
    {
        return version;
    }

    public void setVersion(String version)
    {
        this.version = version;
    }

    public String getCopyrightYear()
    {
        return copyrightYear;
    }

    public void setCopyrightYear(String copyrightYear)
    {
        this.copyrightYear = copyrightYear;
    }

    public boolean isDemoEnabled()
    {
        return demoEnabled;
    }

    public void setDemoEnabled(boolean demoEnabled)
    {
        this.demoEnabled = demoEnabled;
    }

    public static String getProfile()
    {
        return profile;
    }

    public void setProfile(String profile) {
        RuoYiConfig.profile = profile;
    }

    public static boolean isAddressEnabled() {
        return addressEnabled;
    }

    public void setAddressEnabled(boolean addressEnabled) {
        RuoYiConfig.addressEnabled = addressEnabled;
    }

    public static boolean isUploadImageCompressEnabled() {
        return uploadImageCompressEnabled;
    }

    public static double getUploadImageCompressByteLimit() {
        return uploadImageCompressByteLimit;
    }

    public void setUploadImageCompressByteLimit(double uploadImageCompressByteLimit) {
        RuoYiConfig.uploadImageCompressByteLimit = uploadImageCompressByteLimit;
    }

    public void setUploadImageCompressEnabled(boolean uploadImageCompressEnabled) {
        RuoYiConfig.uploadImageCompressEnabled = uploadImageCompressEnabled;
    }

    public static boolean isUploadPdfCompressEnabled() {
        return uploadPdfCompressEnabled;
    }

    public void setUploadPdfCompressEnabled(boolean uploadPdfCompressEnabled) {
        RuoYiConfig.uploadPdfCompressEnabled = uploadPdfCompressEnabled;
    }

    public static double getUploadPdfCompressFactor() {
        return uploadPdfCompressFactor;
    }

    public void setUploadPdfCompressFactor(double uploadPdfCompressFactor) {
        RuoYiConfig.uploadPdfCompressFactor = uploadPdfCompressFactor;
    }

    public static double getUploadPdfCompressLimit() {
        return uploadPdfCompressLimit;
    }

    public void setUploadPdfCompressLimit(double uploadPdfCompressLimit) {
        RuoYiConfig.uploadPdfCompressLimit = uploadPdfCompressLimit;
    }

    /**
     * 获取头像上传路径
     */
    public static String getAvatarPath() {
        return getProfile() + "/avatar";
    }

    /**
     * 获取下载路径
     */
    public static String getDownloadPath()
    {
        return getProfile() + "/download/";
    }

    /**
     * 获取上传路径
     */
    public static String getUploadPath()
    {
        return getProfile() + "/upload";
    }
}
