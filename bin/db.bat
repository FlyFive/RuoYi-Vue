@echo off
echo ctms初始化数据
set username=root
set password=root

echo 导入若依核心SQL
mysql -u%username% -p%password% ctms<D:\Projects\shenchuangit\ruoyi\sql\ry_20200901.sql
mysql -u%username% -p%password% ctms<D:\Projects\shenchuangit\ruoyi\sql\quartz.sql

echo 导入自研扩展SQL
mysql -u%username% -p%password% ctms<D:\Projects\shenchuangit\ruoyi-extensions\sc-enterprise\trunk\src\main\resources\sql\update_1.0.0.sql
mysql -u%username% -p%password% ctms<D:\Projects\shenchuangit\ruoyi-extensions\sc-filemanager\trunk\src\main\resources\sql\update_1.0.0.sql