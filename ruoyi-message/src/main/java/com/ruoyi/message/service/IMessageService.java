package com.ruoyi.message.service;

import com.ruoyi.message.domain.MessageBody;
import com.ruoyi.message.domain.MessageBodyRestDO;
import com.ruoyi.message.menu.MessageStatus;

import java.util.List;

/**
 * 消息service
 *
 * @author liuyu
 */
public interface IMessageService {


    /**
     * 查询推送的用户类型的消息（外部接口推送查询使用）
     *
     * @return 消息列表
     */
    public List<MessageBodyRestDO> selectMessageRestDOList(MessageBodyRestDO messageBodyRestDO);

    /**
     * 发送消息
     *
     * @param messageBody 消息主体
     * @return 返回信息
     */
    public void sendMessage(MessageBody messageBody);

    /**
     * 查询自己收到的消息
     *
     * @return 自己的消息列表
     */
    public List<MessageBody> getMessageListToMyself();

    /**
     * 查询自己收到的消息
     *
     * @param status 状态
     * @return 自己的消息列表
     */
    public List<MessageBody> getMessageListToMyself(MessageStatus status);

    /**
     * 查询自己发送的消息
     *
     * @return 发送的消息列表
     */
    public List<MessageBody> getMessageListSendFromMe();

    /**
     * 通过ID查询消息详情
     *
     * @param id 消息ID
     * @return 消息
     */
    public MessageBody getMessageById(Long id);

    /**
     * 删除消息
     *
     * @param id 消息主体ID
     */
    public void deleteMessageBodyById(Long id);

    /**
     * 标记已读状态
     *
     * @param id 业务ID
     * @return 返回结果
     */
    public int readMark(Long id, String status);

    /**
     * 接收推送的消息
     *
     * @param messageBodyRestDO 消息主体
     * @return 返回信息
     */
    public void receiveRestMessage(MessageBodyRestDO messageBodyRestDO);
}
