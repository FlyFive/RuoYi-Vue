package com.ruoyi.message.controller;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.message.domain.MessageBodyRestDO;
import com.ruoyi.message.service.IMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 接收推送消息 Rest 控制器
 *
 * @author liuyu
 */
@RestController
@RequestMapping("/openapi/message/")
public class MessageRestController extends BaseController {

    @Autowired
    private IMessageService iMessageService;

    /**
     * 发送消息
     *
     * @param messageBodyRestDO 消息主体
     * @return 返回结果
     */
    @PostMapping("/send")
    public AjaxResult send(@RequestBody MessageBodyRestDO messageBodyRestDO) {
        iMessageService.receiveRestMessage(messageBodyRestDO);
        return AjaxResult.success("发送成功");
    }

    /**
     * 查询发给自己的消息
     *
     * @return 消息列表
     */
    @GetMapping("/list")
    public TableDataInfo list(MessageBodyRestDO messageBodyRestDO) {
        startPage();
        List<MessageBodyRestDO> messageBodyList = this.iMessageService.selectMessageRestDOList(messageBodyRestDO);
        return getDataTable(messageBodyList);
    }

}
