package com.ruoyi.message.controller;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.message.domain.MessageBody;
import com.ruoyi.message.menu.MessageStatus;
import com.ruoyi.message.service.IMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 消息控制器
 *
 * @author liuyu
 */
@RestController
@RequestMapping("/system/message/")
public class MessageController extends BaseController {

    @Autowired
    private IMessageService iMessageService;

    /**
     * 查询自己发出的消息
     *
     * @return 消息列表
     */
    @GetMapping("/listFromMe")
    public TableDataInfo listFromMe() {
        startPage();
        List<MessageBody> messageBodyList = this.iMessageService.getMessageListSendFromMe();
        return getDataTable(messageBodyList);
    }

    /**
     * 查询发给自己的消息
     *
     * @return 消息列表
     */
    @GetMapping("/listToMe")
    public TableDataInfo listToMe() {
        startPage();
        List<MessageBody> messageBodyList = this.iMessageService.getMessageListToMyself();
        return getDataTable(messageBodyList);
    }

    /**
     * 查询发给自己的消息（已读）
     *
     * @return 消息列表
     */
    @GetMapping("/listToMeRead")
    public TableDataInfo listToMeRead() {
        startPage();
        List<MessageBody> messageBodyList = this.iMessageService.getMessageListToMyself(MessageStatus.READ);
        return getDataTable(messageBodyList);
    }

    /**
     * 查询发给自己的消息（未读）
     *
     * @return 消息列表
     */
    @GetMapping("/listToMeUnRead")
    public TableDataInfo listToMeUnRead() {
        startPage();
        List<MessageBody> messageBodyList = this.iMessageService.getMessageListToMyself(MessageStatus.UNREAD);
        return getDataTable(messageBodyList);
    }

    /**
     * 根据ID查询消息
     *
     * @param id 消息ID
     * @return 返回结果
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable Long id) {
        return AjaxResult.success(iMessageService.getMessageById(id));
    }

    /**
     * 发送消息
     *
     * @param messageBody   消息主体
     * @param bindingResult 验证
     * @return 返回结果
     */
    @PostMapping("/send")
    public AjaxResult send(@RequestBody MessageBody messageBody, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return AjaxResult.error(bindingResult.getAllErrors().stream().map(ObjectError::getDefaultMessage).collect(Collectors.toList()).toString());
        } else {
            iMessageService.sendMessage(messageBody);
            return AjaxResult.success();
        }
    }

    /**
     * 删除消息
     *
     * @param id 消息ID
     * @return 返回结果
     */
    @DeleteMapping("/{id}")
    public AjaxResult delete(@PathVariable Long id) {
        iMessageService.deleteMessageBodyById(id);
        return AjaxResult.success();
    }

    /**
     * 标记已读
     *
     * @param id 消息ID
     * @return 返回结果
     */
    @PostMapping("/markRead/{id}")
    public AjaxResult markRead(@PathVariable Long id, @RequestParam(value = "status", required = false, defaultValue = "1") String status) {
        return AjaxResult.success(iMessageService.readMark(id, status));
    }
}
