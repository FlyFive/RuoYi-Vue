package com.ruoyi.message.menu;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 消息类型枚举
 *
 * @author liuyu
 */
public enum MessageReceiveType {
    /**
     * 用户消息
     */
    USER("用户消息", 0),
    /**
     * 部门消息
     */
    DEPT("部门消息", 1),
    /**
     * 角色消息
     */
    ROLE("角色消息", 2),
    /**
     * 岗位消息
     */
    POST("岗位消息", 3);

    private String name;
    private Integer value;

    private MessageReceiveType(String name, Integer value) {
        this.name = name;
        this.value = value;
    }

    public static String getName(int index) {
        for (MessageReceiveType status : MessageReceiveType.values()) {
            if (status.getValue() == index) {
                return status.getName();
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("name", getName())
                .append("value", getValue())
                .toString();
    }
}
