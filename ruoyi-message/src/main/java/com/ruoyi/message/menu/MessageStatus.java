package com.ruoyi.message.menu;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 消息状态枚举
 *
 * @author liuyu
 */
public enum MessageStatus {
    /**
     * 已读
     */
    READ("已读", 1),
    /**
     * 未读
     */
    UNREAD("未读", 0);

    private String name;
    private Integer value;

    private MessageStatus(String name, Integer value) {
        this.name = name;
        this.value = value;
    }

    public static String getName(int index) {
        for (MessageStatus status : MessageStatus.values()) {
            if (status.getValue() == index) {
                return status.getName();
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("name", getName())
                .append("value", getValue())
                .toString();
    }
}
