package com.ruoyi.message.domain;

import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * 消息主体表
 *
 * @author liuyu
 */
public class MessageBody extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    private Long id;

    /**
     * 发信人
     */
    @NotBlank(message = "发信人ID不能为空")
    private Long messageFrom;

    /**
     * 消息标题
     */
    @NotBlank(message = "消息标题不能为空")
    @Size(max = 200, message = "消息标题最长200个字符")
    private String title;

    /**
     * 消息正文
     */
    @Size(max = 2000, message = "消息标题最长2000个字符")
    private String content;

    /**
     * 消息类型（0：人，1：部门，2：角色）
     */
    @NotBlank
    private Integer messageType;

    /**
     * 消息接收人
     */
    @NotBlank
    @Size(max = 1000, message = "消息接收主体最长2000个字符")
    private String receiver;

    /**
     * 业务标识
     */
    @Size(max = 50, message = "业务标识最长50个字符")
    private String businessKey;

    /**
     * 组件路径
     */
    @Size(max = 200, message = "业务标识最长200个字符")
    private String componentPath;

    /**
     * 用户ID，acl权限字段未启用临时解决
     */
    private Long userId;
    /**
     * 部门ID，acl权限字段未启用临时解决
     */
    private Long deptId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMessageFrom() {
        return messageFrom;
    }

    public void setMessageFrom(Long messageFrom) {
        this.messageFrom = messageFrom;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getMessageType() {
        return messageType;
    }

    public void setMessageType(Integer messageType) {
        this.messageType = messageType;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getBusinessKey() {
        return businessKey;
    }

    public void setBusinessKey(String businessKey) {
        this.businessKey = businessKey;
    }

    public String getComponentPath() {
        return componentPath;
    }

    public void setComponentPath(String componentPath) {
        this.componentPath = componentPath;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("messageFrom", messageFrom)
                .append("title", title)
                .append("content", content)
                .append("messageType", messageType)
                .append("receiver", receiver)
                .append("businessKey", businessKey)
                .append("componentPath", componentPath)
                .append("userId", userId)
                .append("deptId", deptId)
                .toString();
    }
}
