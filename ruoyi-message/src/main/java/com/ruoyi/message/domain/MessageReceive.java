package com.ruoyi.message.domain;

import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.validation.constraints.NotBlank;

/**
 * 消息接收方
 *
 * @author liuyu
 */
public class MessageReceive extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    private Long id;

    /**
     * 消息ID
     */
    @NotBlank(message = "消息ID不能为空")
    private Long messageId;

    /**
     * 接收人
     */
    @NotBlank(message = "接收人不能为空")
    private Long messageTo;

    /**
     * 消息类型（0：人，1：部门，2：角色）
     */
    @NotBlank
    private Integer status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMessageId() {
        return messageId;
    }

    public void setMessageId(Long messageId) {
        this.messageId = messageId;
    }

    public Long getMessageTo() {
        return messageTo;
    }

    public void setMessageTo(Long messageTo) {
        this.messageTo = messageTo;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("messageId", getMessageId())
                .append("messageTo", getMessageTo())
                .append("status", getStatus())
                .toString();
    }
}
