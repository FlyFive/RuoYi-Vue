package com.ruoyi.message.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;

/**
 * 接收消息 消息主体 DO
 */
public class MessageBodyRestDO {

    /**
     * id
     */
    private Long id;

    /**
     * 发信人
     */
    private String messageFrom;

    /**
     * 消息标题
     */
    private String title;

    /**
     * 消息正文
     */
    private String content;

    /**
     * 消息类型（0：人，1：部门，2：角色）
     */
    private Integer messageType;

    /**
     * 消息接收人
     */
    private String receiver;

    /**
     * 创建者
     */
    private String createBy;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /**
     * 创建者ID（权限使用）
     */
    private Long aclCreateById;
    /**
     * 页码
     */
    @JsonIgnore
    private String pageNum;
    /**
     * 页大小
     */
    @JsonIgnore
    private String pageSize;

    public MessageBody toMessageBody() {
        MessageBody messageBody = new MessageBody();
        if (null != this.getMessageType()) {
            messageBody.setMessageType(this.getMessageType());
        }
        if (null != this.getId()) {
            messageBody.setId(this.getId());
        }
        if (null != this.getTitle()) {
            messageBody.setTitle(this.getTitle());
        }
        if (null != this.getContent()) {
            messageBody.setContent(this.getContent());
        }
        if (null != this.getMessageType()) {
            messageBody.setMessageType(this.getMessageType());
        }
        if (null != this.getCreateTime()) {
            messageBody.setCreateTime(this.getCreateTime());
        }
        if (null != this.getCreateBy()) {
            messageBody.setCreateBy(this.getCreateBy());
        }
        if (null != this.getAclCreateById()) {
            messageBody.setAclCreateById(this.getAclCreateById());
        }
        return messageBody;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMessageFrom() {
        return messageFrom;
    }

    public void setMessageFrom(String messageFrom) {
        this.messageFrom = messageFrom;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getMessageType() {
        return messageType;
    }

    public void setMessageType(Integer messageType) {
        this.messageType = messageType;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Long getAclCreateById() {
        return aclCreateById;
    }

    public void setAclCreateById(Long aclCreateById) {
        this.aclCreateById = aclCreateById;
    }

    public String getPageNum() {
        return pageNum;
    }

    public void setPageNum(String pageNum) {
        this.pageNum = pageNum;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }
}
