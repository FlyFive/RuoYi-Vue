package com.ruoyi.message.mapper;

import com.ruoyi.message.domain.MessageBody;
import com.ruoyi.message.domain.MessageBodyRestDO;
import com.ruoyi.message.domain.MessageReceive;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 消息Mapper
 *
 * @author liuyu
 */
@Repository
public interface MessageMapper {

    /**
     * 通过ID查询消息详情
     *
     * @param id 消息ID
     * @return 消息
     */
    MessageBody selectMessageBodyById(Long id);

    /**
     * 查询自己收到的消息
     *
     * @return 自己的消息列表
     */
    List<MessageBody> selectMessageListToMyself(@Param("userId") Long userId);

    /**
     * 查询自己收到的消息
     *
     * @param status 状态
     * @return 自己的消息列表
     */
    List<MessageBody> getMessageListToMyself(@Param("userId") Long userId, @Param("status") Integer status);

    /**
     * 查询发送的消息
     *
     * @return 发送的消息列表
     */
    List<MessageBody> selectMessageList(MessageBody messageBody);

    /**
     * 查询推送的用户类型的消息（外部接口推送查询使用）
     *
     * @return 发送的消息列表
     */
    List<MessageBodyRestDO> selectMessageRestDOList(MessageBodyRestDO messageBodyRestDO);

    /**
     * 保存消息主体
     *
     * @param messageBody 消息主体
     * @return 返回信息
     */
    int insertMessageBody(MessageBody messageBody);

    /**
     * 保存接收人信息
     *
     * @param messageReceive 收件人
     * @return 返回信息
     */
    int insertMessageReceive(MessageReceive messageReceive);

    /**
     * 批量保存保存收件人信息
     *
     * @param messageReceiveList 收件人
     * @return 返回信息
     */
    int insertBatchMessageReceive(@Param("messageReceiveList") List<MessageReceive> messageReceiveList);

    /**
     * 删除消息
     *
     * @param id 消息主体ID
     */
    void deleteMessageBodyById(Long id);

    /**
     * 通过消息ID删除消息接收人
     *
     * @param messageId 消息ID
     */
    void deleteMessageReceiveByMessageId(Long messageId);

    /**
     * 获取根据岗位获取到所有用户ID
     *
     * @param postId 岗位ID
     * @return 用户ID list
     */
    List<Long> selectUserListByPostId(@Param("postId") Long postId);

    /**
     * 通过角色key查询所有用户ID
     *
     * @param roleId 角色ID
     * @return 用户ID list
     */
    List<Long> selectUserListByRoleId(@Param("roleId") Long roleId);

    /**
     * 通过部门ID查询所有用户ID
     *
     * @param deptId 部门ID
     * @return 用户ID list
     */
    List<Long> selectUserListByDeptId(@Param("deptId") Long deptId);

    /**
     * 通过部门ID查询自身及下属所有用户ID
     *
     * @param deptId 部门ID
     * @return 用户ID list
     */
    List<Long> selectUserListByDeptIdRecursive(@Param("deptId") Long deptId);

    /**
     * 标记已读状态
     *
     * @param messageId 消息ID
     * @param messageTo 消息接收人ID
     * @param status    消息状态
     * @return 返回结果
     */
    int readMark(@Param("messageId") Long messageId, @Param("messageTo") Long messageTo, @Param("status") String status);

    /**
     * 根据用户名查询用户ID
     *
     * @param username 用户名
     * @return ID集合
     */
    Long selectUserIdListByUserName(@Param("username") String username);

    /**
     * 根据多个用户名查询用户ID
     *
     * @param usernames 用户名集合
     * @return ID集合
     */
    List<Long> selectUserIdListByUserNames(String[] usernames);

    /**
     * 根据多个用户ID查询用户名
     *
     * @param userId 用户ID集合
     * @return 用户名集合
     */
    List<String> selectUserNameListByUserIds(Long[] userId);
}
