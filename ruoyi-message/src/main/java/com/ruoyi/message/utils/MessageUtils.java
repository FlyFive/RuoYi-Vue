package com.ruoyi.message.utils;

import com.ruoyi.common.utils.spring.SpringUtils;
import com.ruoyi.message.domain.MessageBody;
import com.ruoyi.message.service.IMessageService;

import java.util.TimerTask;

/**
 * 发送通知工具类
 *
 * @author liuyu
 */
public class MessageUtils {

    /**
     * 发送消息
     *
     * @param messageBody 消息主体
     */
    public static void sendMessage(MessageBody messageBody) {
        SpringUtils.getBean(IMessageService.class).sendMessage(messageBody);
    }

    /**
     * 异步发送消息（产生任务）
     *
     * @param messageBody 消息主体
     * @return 异步任务
     */
    public static TimerTask sendMessageAsync(MessageBody messageBody) {
        return new TimerTask() {
            @Override
            public void run() {
                SpringUtils.getBean(IMessageService.class).sendMessage(messageBody);
            }
        };
    }
}
